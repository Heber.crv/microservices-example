//The gateway only returns a status: ok response if it is accessed on his root directory
const express=require('express');
const app=express();
const http = require('http');

app.get('/', (req,res) => {
	res.json('{"status": "ok"}');
});

//sends get to microservice 1 that returns the current date and time
app.get('/micro1', (req,res) => {
	http.get('http://micro1:3001', (resp) => {
	let data = '';

  	resp.on('data', (chunk) => {
    		data += chunk;
  	});
  	resp.on('end', () => {
		res.json(JSON.parse(data));

  	});
	}).on("error", (err) => {
  		console.log("Error: " + err.message);
	});
});

app.get('/micro2', (req,res) => {
	http.get('http://micro2:3002', (resp) => {
  	let data = '';

  	resp.on('data', (chunk) => {
    		data += chunk;
  	});
  	resp.on('end', () => {
		res.json(JSON.parse(data));

  	});
	}).on("error", (err) => {
  		console.log("Error: " + err.message);
	});
});

app.listen('3000',()=>{
	console.log('Listening on port 3000');
});
