#!/bin/bash
#script used to stop and delete all the containers, network and images created

docker-compose stop
docker-compose down --rmi all

#docker container stop micro-gw
#docker container rm micro-gw
#docker rmi api-gateway:latest

#docker container stop micro1
#docker container rm micro1
#docker rmi micro1:latest

#docker container stop micro2
#docker container rm micro2
#docker rmi micro2:latest

#docker network rm api-net
