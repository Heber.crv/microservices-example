const express=require('express');
const app=express();

// returns the current date and time of this server
app.get('/', (req,res) => {
  let date_ob = new Date();

  //adjust 0 before single digit date
  let date = ("0" + date_ob.getDate()).slice(-2);

  //month
  let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  //year
  let year = date_ob.getFullYear();

  //hours
  let hours = date_ob.getHours();

  //minutes
  let minutes = date_ob.getMinutes();

  //seconds
  let seconds = date_ob.getSeconds();

	response='{"date":'+'"'+year+'-'+month+'-'+date+'","time":"'+hours +':'+minutes+':'+seconds+'"}';
  res.json(response);
});

//listens on port 3001 to receive requests
app.listen('3001',()=>{
	console.log('Listening on port 3001');
});
