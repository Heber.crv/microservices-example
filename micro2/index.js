const express=require('express');
const app=express();
const os=require('os');
const hostname=os.hostname();

//returns the hostname of this server
app.get('/', (req,res) => {
  response='{"hostname":'+'"'+hostname+'"}'
	res.json(response);
});

app.listen('3002',()=>{
	console.log('Listening on port 3002');
});
